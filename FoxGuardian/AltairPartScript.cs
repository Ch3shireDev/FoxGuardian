﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AltairPartScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        var player = other.GetComponent<PlayerScript>();
        if (player == null) return;
        player.AddAltairPart();
        Destroy(gameObject);
    }
}
