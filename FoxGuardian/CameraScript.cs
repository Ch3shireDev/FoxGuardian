﻿using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class CameraScript : MonoBehaviour
{
    //public float bloom = 10f;
    public Vector2 MaxDistance = new Vector2(10, 10);
    public float MaxHorizontal = 1000;
    public float MaxVertical = 0;

    public float MinHorizontal = -48;
    public float MinVertical = 0;

    public GameObject Player;
    //public float saturation = 5f;

    private void Update()
    {
        var playerPos = Player.transform.position;
        var cameraPos = transform.position;

        var x0 = playerPos.x;
        var x = cameraPos.x;

        var y0 = playerPos.y;
        var y = cameraPos.y;

        if (Mathf.Abs(x - x0) > MaxDistance.x) x = x0 + MaxDistance.x * Mathf.Sign(x - x0);

        if (Mathf.Abs(y - y0) > MaxDistance.y) y = y0 + MaxDistance.y * Mathf.Sign(y - y0);

        if (y > MaxVertical) y = MaxVertical;
        else if (y < MinVertical) y = MinVertical;

        if (x < MinHorizontal) x = MinHorizontal;
        else if (x > MaxHorizontal) x = MaxHorizontal;

        cameraPos.x = x;
        cameraPos.y = y;

        transform.position = cameraPos;

        var sat = GetSaturation();
        if (sat > -99)
        {
            SetSaturation(sat - 0.1f);
        }

    }

    public void SetSaturation(float saturation)
    {
        var volume = gameObject.GetComponent<PostProcessVolume>();
        volume.profile.TryGetSettings(out ColorGrading colorGradingLayer);
        if (colorGradingLayer == null) return;
        colorGradingLayer.enabled.value = true;
        colorGradingLayer.saturation.value = saturation;
    }

    public float GetSaturation()
    {
        var volume = gameObject.GetComponent<PostProcessVolume>();
        //volume.profile.TryGetSettings(out Bloom bloomLayer);
        //volume.profile.TryGetSettings(out AmbientOcclusion ambientOcclusionLayer);
        volume.profile.TryGetSettings(out ColorGrading colorGradingLayer);
        if (colorGradingLayer == null) return 0;
        var saturation = colorGradingLayer.saturation.value;
        return saturation;
    }
}