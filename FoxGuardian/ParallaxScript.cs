﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxScript : MonoBehaviour
{
    GameObject Camera;
    //public GameObject Player;

    private Vector3 initialPosition;
    // Start is called before the first frame update
    void Start()
    {
        Camera = GameObject.Find("Main Camera");
        //Player = GameObject.Find("FoxPlayer");
        initialPosition = transform.position;
    }

    public float HorizontalSpeed = 1;
    public float VerticalSpeed = 1;

    // Update is called once per frame
    void Update()
    {
        var cameraPos = Camera.transform.position;
        var cameraHorizontal = cameraPos.x;


        var pos = transform.position;
        pos.x = cameraPos.x + (initialPosition.x - cameraPos.x)*HorizontalSpeed;
        pos.y = cameraPos.y + (initialPosition.y - cameraPos.y) * VerticalSpeed;
        transform.position = pos;
    }
}
