﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour
{
    public int AllAltarParts = 3;
    public int AllWisps = 20;

    public int AltarPartCounter;
    public Text AltarPartText;

    public bool fromTree;

    private BoxCollider2D groundDetect;
    public float HorizontalAcceleration = 10.0f;
    public float JumpForce = 1.0f;
    public Vector2 JumpFromTreeForce = new Vector2(10, 5);

    public float MaxHorizontalSpeed = 1.0f;

    private Vector2 NormalTreeVector;
    public bool onGround;
    public bool onTree;
    private Rigidbody2D physics;
    private SpriteRenderer sprite;

    public int WispCounter;

    public Text WispText;

    public float GravityScaleOnTree = 0.7f;

    private void Start()
    {
        physics = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
        groundDetect = GetComponentInChildren<BoxCollider2D>();
        InitialGravityScale = physics.gravityScale;
    }

    private void Update()
    {
        var horizontal = Input.GetAxis("Horizontal");
        var velocity = physics.velocity;

        if (fromTree)
        {
            var normalSign = Mathf.FloorToInt(Mathf.Sign(NormalTreeVector.x));
            var horizontalSign = Mathf.FloorToInt(Mathf.Sign(horizontal));
            if (Mathf.Abs(horizontal) < 0.01f) horizontalSign = 0;
            if (horizontalSign != 0)
                if (normalSign != horizontalSign)
                    velocity.x = horizontal * MaxHorizontalSpeed;
        }
        else
        {
            if (onGround)
                velocity.x = horizontal * MaxHorizontalSpeed;
            else
                velocity.x += horizontal * HorizontalAcceleration;

            if (Mathf.Abs(velocity.x) > MaxHorizontalSpeed) velocity.x = Mathf.Sign(velocity.x) * MaxHorizontalSpeed;
        }

        physics.velocity = velocity;

        if (horizontal > 0.01f && !sprite.flipX) sprite.flipX = true;

        if (horizontal < -0.01f && sprite.flipX) sprite.flipX = false;

        if (Input.GetButton("Jump"))
        {
            if (onGround && Input.GetButtonDown("Jump"))
            {
                physics.AddForce(Vector2.up * JumpForce, ForceMode2D.Impulse);
                onGround = false;
            }
            else if (onTree)
            {
                var normalSign = Mathf.FloorToInt(Mathf.Sign(NormalTreeVector.x));
                var horizontalSign = Mathf.FloorToInt(Mathf.Sign(horizontal));
                if (Mathf.Abs(horizontal) < 0.01f) horizontalSign = 0;

                if (normalSign == horizontalSign)
                {
                    var velocity2 = physics.velocity;
                    velocity2.x = NormalTreeVector.x * JumpFromTreeForce.x;
                    velocity2.y = JumpFromTreeForce.y;
                    physics.velocity = velocity2;
                    onTree = false;
                    fromTree = true;
                }
            }
        }

        var overlapCube = transform.Find("GroundArea");
        var bounds = overlapCube.GetComponent<MeshRenderer>().bounds;
        var pointA = bounds.min;
        var pointB = bounds.max;
        var overlap = Physics2D.OverlapArea(pointA, pointB);
        //var isTouching = overlap.IsTouchingLayers(LayerMask.NameToLayer("Ground"));
        //Debug.Log(pointA);

        if (overlap != null)
        {
            onGround = true;
            fromTree = false;
        }


        if (onTree)
        {
            physics.gravityScale = InitialGravityScale * GravityScaleOnTree;
        }
        else
        {
            physics.gravityScale = InitialGravityScale;
        }
    }

    private float InitialGravityScale = 1;

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.layer == LayerMask.NameToLayer("Tree") && !onTree)
        {
            fromTree = false;
            onTree = true;
            NormalTreeVector = collision.contacts[0].normal;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Ground") && onGround) onGround = false;

        if (collision.gameObject.layer == LayerMask.NameToLayer("Tree") && onTree) onTree = false;
    }

    public void AddWisp()
    {
        WispCounter++;
        WispText.text = $"{WispCounter} / {AllWisps}";

        var camera = GameObject.Find("Main Camera");
        var cameraScript = camera.GetComponent<CameraScript>();
        cameraScript.SetSaturation(0);
    }

    public void AddAltairPart()
    {
        AltarPartCounter++;
        AltarPartText.text = $"{AltarPartCounter} / {AllAltarParts}";
    }
}