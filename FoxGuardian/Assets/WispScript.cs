﻿using UnityEngine;

public class WispScript : MonoBehaviour
{
    private bool block = false;

    private float time = 0;

    Vector3 InitialPos;

    void Start()
    {
        InitialPos = gameObject.transform.position;
    }

    void Update()
    {
        time += Time.deltaTime;
        gameObject.transform.position = InitialPos + Vector3.up * 1f * Mathf.Sin(time / 2);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (block) return;
        var collectItem = collider.gameObject.GetComponent<PlayerCollectItem>();
        if (collectItem == null) return;
        block = true;
        collectItem.AddWisp();
        Destroy(gameObject);
    }
}
