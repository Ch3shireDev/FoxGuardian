﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AltarPartScript : MonoBehaviour
{
    private bool block = false;
    void OnTriggerEnter2D(Collider2D collider)
    {
        if (block) return;
        var collectItem = collider.GetComponent<PlayerCollectItem>();
        if (collectItem == null) return;
        block = true;
        collectItem.AddAltar();
        Destroy(gameObject);
    }

}
