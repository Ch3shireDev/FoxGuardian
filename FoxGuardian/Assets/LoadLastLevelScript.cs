﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadLastLevelScript : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collider)
    {
        SceneManager.LoadScene(2);
    }
}