﻿using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.SceneManagement;

public class DimScreen : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public float TimeDelay = 20;
    public float DimSpeed = 50;
    public bool QuitAfterTime = false;

    private float time = 0;
    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        if (time < TimeDelay) return;
        var volume = GetComponent<PostProcessVolume>();
        var color = volume.profile.GetSetting<ColorGrading>();
        color.postExposure.overrideState = true;

        var val = DimSpeed * (time - TimeDelay);
        if (val < 20.0f)
        {
            color.postExposure.value = -val;
        }

        else
        {
            if (QuitAfterTime)
            {
                Debug.Log("Am quitting");
                Application.Quit();
                if (Application.isEditor)
                {
                    //UnityEditor.EditorApplication.isPlaying = false;
                }
            }
            else
            {
                SceneManager.LoadScene(1);
            }
        }
    }
}
