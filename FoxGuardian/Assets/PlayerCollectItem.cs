﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCollectItem : MonoBehaviour
{

    public int MaxWispNumber = 20;
    public int MaxAltarNumber = 5;

    private int WispNumber = 0;
    private int AltarNumber = 0;

    public void AddWisp()
    {
        WispNumber++;
        var text = GameObject.Find("WispText").GetComponent<Text>();
        text.text = $"{WispNumber} / {MaxWispNumber}";
    }

    public void AddAltar()
    {
        AltarNumber++;
        var text = GameObject.Find("AltarText").GetComponent<Text>();
        text.text = $"{AltarNumber} / {MaxAltarNumber}";
    }
}
