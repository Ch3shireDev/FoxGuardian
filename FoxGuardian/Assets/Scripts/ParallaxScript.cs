﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxScript : MonoBehaviour
{
    GameObject Camera;

    private Vector3 SpriteInitialPosition;

    private Vector3 CameraInitialPosition;

    void Start()
    {
        Camera = GameObject.Find("Main Camera");

        CameraInitialPosition = Camera.transform.position;
        SpriteInitialPosition = transform.position;
    }

    public float HorizontalSpeed = 1;
    public float VerticalSpeed = 1;

    // Update is called once per frame
    void Update()
    {
        var cameraPos = Camera.transform.position;
        var cameraHorizontal = cameraPos.x;


        var pos = transform.position;
        pos.x = SpriteInitialPosition.x + (CameraInitialPosition.x - cameraPos.x)*HorizontalSpeed;
        pos.y =  SpriteInitialPosition.y + (CameraInitialPosition.y - cameraPos.y) * VerticalSpeed;
        transform.position = pos;
    }
}
