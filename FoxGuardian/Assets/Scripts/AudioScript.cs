﻿using UnityEngine;

public class AudioScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<AudioSource>().PlayDelayed(StartAfterTime);
    }

    public float StartAfterTime = 5.0f;
    //private float time = 0;

    public AnimationCurve VolumeAfterTime;

    // Update is called once per frame
    void Update()
    {
        var audio = GetComponent<AudioSource>();
        if (audio.isPlaying)
        {
            var time = audio.time;
            var volume = VolumeAfterTime.Evaluate(time);
            audio.volume = volume;
        }
    }
}
