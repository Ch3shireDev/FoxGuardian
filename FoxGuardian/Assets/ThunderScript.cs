﻿using System.Collections.Generic;
using System.Diagnostics.Tracing;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class ThunderScript : MonoBehaviour
{
    public float DelayTime = 10f;

    private bool isRunning = false;
    private bool hasEnded = false;


    public List<GameObject> ObjectsToRemove;
    public List<GameObject> ObjectsToShow;

    public float TotalTime = 1f;

    private Bloom bloom;

    private void Start()
    {
        GetComponent<SpriteRenderer>().enabled = false;
        if (Camera == null) return;
        var volume = Camera.GetComponent<PostProcessVolume>();
        bloom = volume.profile.GetSetting<Bloom>();
    }

    private float timeEnded;

    private void Update()
    {
        if (Time.time > DelayTime) StartRoutine();

        ContinueRoutine();

        if (hasEnded && bloom != null)
        {
            bloom.intensity.value = 100 / (1 + timeEnded*10);
            timeEnded += Time.deltaTime;
        }
    }

    private void StartRoutine()
    {
        if (isRunning || hasEnded) return;
        isRunning = true;
        GetComponent<SpriteRenderer>().enabled = true;
        GetComponent<AudioSource>().Play();
    }

    private float continueTime = 0;
    private void ContinueRoutine()
    {
        if (!isRunning) return;
        continueTime += Time.deltaTime;

        if (bloom != null)
        {
            bloom.intensity.overrideState = true;
            bloom.intensity.value = continueTime * 500;
        }

        if (continueTime > TotalTime)
        {
            EndRoutine();
        }
    }

    public GameObject Camera;

    private void EndRoutine()
    {
        isRunning = false;
        hasEnded = true;
        GetComponent<SpriteRenderer>().enabled = false;
        foreach (var element in ObjectsToRemove) element.SetActive(false);

        foreach (var element in ObjectsToShow) element.SetActive(true);


    }
}