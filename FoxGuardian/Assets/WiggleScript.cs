﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WiggleScript : MonoBehaviour
{
    private float InitialZ;

    void Start()
    {
        InitialZ = transform.rotation.eulerAngles.z;
    }

    void Update()
    {
        var rotation = transform.rotation.eulerAngles;
        rotation.z = InitialZ + Amplitude * Mathf.Sin(2 * Mathf.PI / Period * Time.time);
        transform.rotation = Quaternion.Euler(rotation);
    }

    public float Amplitude = 20f;
    public float Period = 20f;

}
