# Fox Guardian

Prosta gra typu sidescroller o lisim duchu opiekuńczym który musi odzyskać fragmenty ołtarza wrócić na plan astralny.

# Zadania do wykonania

## Cheshire 

- [x] Fizyka lisa - bieganie, skakanie, skoki na drzewa
- [x] Kamera - śledzenie lisa
- [x] Zbieranie ogników oraz zbieranie fragmentów ołtarza
- [x] UI - liczba fragmentów ołtarza, liczba odzyskanych ogników
- [x] Efekt paralaksy - różne części backgroundu przesuwają się z różnymi prędkościami
- [x] Post-processing ogników - bez ogników ekran traci barwy, po odzyskaniu ognika świat ponownie nabiera barw
- [x] Animacja pierwszej sceny - uderzenie pioruna, rozlecenie się ołtarza na kilka części w różnych kawałkach mapy
- [ ] (opcjonalne) efekt wstęgi Mobiusa
- [ ] Strzałka wskazująca aktualny cel (opcjonalne)

## Gerrd

- [x] Rysunki elementów tła podlegających efektowi paralaksy.
- [x] Platformy podłoża, drzew, gałęzi
- [x] Animacja lisa - idle, bieg, skok, zawieszenie na drzewie, skok z drzewa, spawn i despawn.
- [x] Efekt pioruna i rozlatującego się ołtarza
- [x] Fragmenty ołtarza - kawałki kamienia i lśniący element 
- [ ] Ogniki i animacja ogników
- [x] Wronie gniazdo - opcjonalnie sama wrona ze lśniącym elementem ołtarza
- [x] UI - strzałka
- [x] UI, fragmenty, ogniki


# Video

- https://www.youtube.com/embed/XPNWjXpC2Po
- https://www.youtube.com/embed/s-7VLFTyVQo